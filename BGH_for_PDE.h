template <class D>
struct BGH {
// Variables.
public:
	int kmax; // Maximum method tier.
	Vdoub xout; // Vector storing next argument values.
	Mdoub yout; // Solution matrix.
	int nstp; // Current step number. At the end - number of steps taken.
	int nbad; // Number of steps needed to return to initial values.

// Procedures.
	BGH(double h00, double hminn, int Nn, Vdoub rtoll, double x1, double x2, Vdoub y0, Vdoub r0, D &derivss, bool jacc, bool contt);
	void integrate();
	void output();

private:
	double x; // Current x value.
	double xk; // End of integration scope.
	Vdoub y; // y values vector.
	Vdoub r; // Net vector.
	Mdoub dfdy; // Funkction Jacobian.
	int Nvar; // Number of variables = number of equations.
	Vdoub rtol; // Error tolerance.
	typename D &derivs;
	double h;
	double hmin;
	double etanewt;
	int N; // Number of output points.
	bool jac; // Is Jacobian defined in the integrated function?
	bool cont; // Are the calculations continued or started from beginning?

	// Helper variables.
	Mdoub yt; // temporary solution values for kmax last steps.
	Vdoub ga; // Predictor coefficients.
	Vdoub al; // Corrector coefficients.
	Mdoub xdif; // Argument difference matrix x(n-i)-x(n-j), i=j=0...kmax+1
};

template <class D>
BGH<D>::BGH(double h00, double hminn, int Nn, Vdoub rtoll, double x1, double x2, Vdoub y0, Vdoub r0, typename D &derivss, bool jacc, bool contt) : h(h00), hmin(hminn), N(Nn), xk(x2), derivs(derivss), jac(jacc), cont(contt), r(r0), y(y0) {
	
	kmax=6; // Maximum method tier.
	nbad=0;
	nstp=0;
	etanewt=rtoll(0)*1e-01;

	int i,j;
	Nvar=y0.size();
	rtol.resize(Nvar);
	rtol=rtoll;
	x=x1;
	xdif.resize(kmax+2,kmax+2);
	yout.resize(Nvar, N);
	xout.resize(N);
	ga.resize(kmax+1);
	al.resize(kmax+1);
	dfdy.resize(Nvar, Nvar);
	yt.resize(kmax+1,Nvar);

	for (i=0; i<kmax+2; i++) {
		for (j=0; j<kmax+2; j++) {
			if (i!=j) xdif(i,j)=h;
		}
	}
	for (i=0; i<Nvar; i++) {
		yout(i,0)=y0(i);
		yt(0,i)=y0(i);
	}
	for (i=0; i<Nvar; i++) {
		yout(i,0)=y0(i);
	}
	for (i=0; i<N; i++) xout(i)=x1+i*(x2-x1)/(N-1);
	//if ((xout(1)-xout(0))<=h) throw ("Too high h0 value or too many output points.");
}

template <class D>
void BGH<D>::integrate() {

	cout << "Integration started" << endl << endl;
	const double Coef1=1.2;
	const double ekmax=1.5;
	int k=1;
	double hold;
	int kold;
	int n=1;
	bool Control=0;
	double ek=0; // Integration step change coefficient.
	double pom;
	Vdoub yP(Nvar), temp1(Nvar);
	int NNewt=1;
	int Ncor=5; // Maksimum number of corrector iterations.
	Vdoub ekold(Ncor);
	Vdoub ek1(kmax);
	int i,j,l;
	Vdoub err(Nvar);
	int nk=-1; // Number of steps for the same method tier.
	int Nr=Nvar/2;
	bool begin=1;

	if (cont==0) {
		ofstream output ("BGH.txt", ios::out | ios::trunc);
		output.close();
		/*output << "n " << "h " << "k " << "x ";
		for (i=0; i<Nvar; i++) {
			output << "y(" << i << ") ";
		}
		output << endl;*/
	}

	else {  // If continuing, read values from files.

		ifstream param ("param.txt", ios::out | ios::trunc);
		param >> nstp;
		param >> n;
		param >> k;
		param >> h;
		param >> nk;
		param >> x;
		param.close();

		ifstream ytsave ("yt.txt", ios::out | ios::trunc);
		for (j=0; j<kmax; j++) {
			for (i=0; i<Nvar; i++) ytsave >> yt(j,i);
		}
		ytsave.close();

		ifstream youtsave ("yout.txt", ios::out | ios::trunc);
		for (j=0; j<Nvar; j++) {
			for (i=0; i<N; i++) youtsave >> yout(j,i);
		}
		youtsave.close();

		ifstream xdifsave ("xdif.txt", ios::out | ios::trunc);
		for (j=0; j<kmax+2; j++) {
			for (i=0; i<kmax+2; i++) xdifsave >> xdif(j,i);
		}
		xdifsave.close();
	}

	ofstream output ("BGH.txt", ios::out | ios::app);

	// Save the first point to a file.

	output << nstp << " " << h << " ";
	output << k << " " << x << " ";
	for (int j=0; j<Nvar; j++) {
		output << y(j) << " ";
	}
	for (int j=2; j<Nr+2; j++) {
		output << r(j) << " ";
	}

	output << endl;

	nstp=1;


Start1:

	x+=h;
	if (Control==0) {
				
		// Update xdif matrix.
		for (i=kmax+1; i>0; i--) {
			for (int j=kmax+1; j>0; j--) {
				xdif(i,j)=xdif(i-1,j-1);
			}
		}
		for (i=kmax+1; i>0; i--) {
			xdif(0,i)=xdif(0,i-1)+h;
			xdif(i,0)=(-xdif(0,i));
		}
	}
	if (Control==1) {
		for (i=kmax+1; i>0; i--) {
			xdif(0,i)+=(-hold+h);
			xdif(i,0)=-xdif(0,i);
		}
		nbad++;
		Control=0;
	}

//	xdif.display();
//	yt.display();

	// Calculate coefficients.
	
	for (i=1; i<=k; i++) {
		ga(i)=1;
		for (j=0; j<=k; j++) {
			if (i!=j) {
				ga(i)*=xdif(0,j+1)/xdif(i+1,j+1);
			}
		}
	}

	for (i=1; i<=k; i++) {
		al(i)=xdif(0,1)/xdif(0,i);
		for (j=1; j<=k; j++) {
			if (i!=j) {
				al(i)*=xdif(0,j)/xdif(i,j);
			}
		}
	}

	ga(0)=1;
	for (i=0; i<k; i++) {
		ga(0)-=ga(i+1);
	}

	al(0)=0;
	for (i=1; i<=k; i++) {
		al(0)-=al(i);
	}

	if (begin==1) {
		ga(0)=1;
		ga(1)=0;
	}

	//ga.display();
	//al.display();

	// Predictor.

	for (i=0; i<Nvar; i++) {
		y(i)=0;
		for (j=0; j<=k; j++) {
			y(i)+=ga(j)*yt(j,i);
		}
	}

	yP=y;

	// Corrector.
	for (i=0; ; i++) {

	// Calculate Jacobian.
		Vdoub hy(Nvar), temp2(Nvar), temp3(Nvar), temp4(Nvar), d(Nvar);

		if (jac==1)
			dfdy=derivs.jacobian(h,y);
		else {
		
			derivs(x,y,r,temp1);
			for (j=0; j<Nvar; j++) {
				hy(j)=temp1(j)*h*0.001;
				if (hy(j)==0) hy(j)=1e-50;
			}
			for (j=0; j<Nvar; j++) {
				for (l=0; l<Nvar; l++) {
					if (l==j) temp2(l)=y(l)+hy(l);
					else temp2(l)=y(l);
				}
				for (l=0; l<Nvar; l++) {
					if (l==j) temp3(l)=y(l)-hy(l);
				else temp3(l)=y(l);
				}
				derivs(x,temp2,r,temp4);
				derivs(x,temp3,r,temp2);
				for (l=0; l<Nvar; l++) {
					dfdy(l,j)=(temp4(l)-temp2(l))/(2*hy(j));
				}
			}
		}

		// Calculate a point.

		for (j=0; j<Nvar; j++) {
			dfdy(j,j)+=al(0)/h;
		}

		LUdcmp alu(dfdy);
		
		for (j=0; j<Nvar; j++) {
			temp2(j)=-temp1(j)-(al(0)*y(j))/h;
			for (l=0; l<k; l++) {
				temp2(j)-=(al(l+1)*yt(l,j))/h;
			}
		}
		alu.solve(temp2,d);

		for (j=0; j<Nvar; j++) {
			y(j)+=d(j);
		}



		// Calculate error.
		if (begin==0) {
			derivs(x,y,r,temp1);

			for (j=0; j<Nvar; j++) {
				err(j)=pow((rtol(j)*xdif(0,k+1))/(abs(y(j)-yP(j))),(1.0/k));
			}

			l=err.min();
			for (j=0; j<Nvar; j++) {
				temp4(j)=abs(d(j));
			}
			l=temp4.max();
			cout << "correction no " << i+1 << "  d(" << l << ")=" << d(l) ;
			ek=err(l);
			ekold(i)=ek;
			cout << "  ek(" << l << ")=" << ek << endl;
			if (i==Ncor-1) {
				if (temp4(l)>etanewt && ek>1) ek=0.5*Coef1;
				if (ekold(i)<ekold(i-1) && ekold(i-1)<ekold(i-2)) {
					for (j=0; j<Ncor-1; j++) {
						if (ekold(j)<0) {
							ek=ekold(j);
							break;
						}
					}
				}
				break;
			}
			if (temp4(l)<etanewt) {
				break;
			}
		}
		else {
					
			for (j=0; j<Nvar; j++) {
				temp4(j)=abs(d(j));
			}
			l=temp4.max();
			cout << "correction nr " << i+1 << "  d(" << l << ")=" << d(l) << endl ;
			if (temp4(l)<etanewt*1e-03) break;
			else if (i==(Ncor+10)) {
				x-=h;
				h*=0.5;
				cout << "Reducing initial integration step to h=" << h << endl;
				goto Start1;
			}
			pom=temp4(l);
		}
	}
	
/*
//**********************************************************
		err=temp4;
		cout << "Obliczam optymalne rozmieszczenie punktow siatki." << endl;
		Vdoub err1(Nr);
		double maxdecr=5;
		int Mr=r.size();

		for (i=0; i<Nr; i++)
			err1(i)=0.5*(err(i)+err(i+Nr));

		double temp=err1(err1.min());

		for (i=0; i<Nr; i++)
			err1(i)/=temp;

		for (i=0; i<Nr; i++)
			if (err1(i)>maxdecr) err1(i)=maxdecr;

		//Wyg�adzanie
		for (j=0; j<10; j++) {
			Vdoub err2(Nr);
			err2(0)=err1(0);
			for (i=1; i<Nr-1; i++)
				err2(i)=0.25*err1(i-1)+0.5*err1(i)+0.25*err1(i+1);
			err2(Nr-1)=err1(Nr-1);
			err1=err2;
		}

		err1.display();
		for (i=1; i<Nr; i++) 
			err1(i)+=err1(i-1);

		double acoef;
		double bcoef;

		acoef=(r(Mr-3)-r(2))/(err1(Nr-1)-err1(0));
		bcoef=r(2)-acoef*err1(0);
		
		Vdoub rtemp(Nr);
		for (i=0; i<Nr; i++)
			rtemp(i)=err1(i)*acoef+bcoef;

		Vdoub yttemp(Nr);
		for (i=0; i<Nr; i++)
			yttemp(i)=abs(rtemp(i)-r(i+2));

		i=yttemp.max();

		if (yttemp(i)>(0.2*((r(Mr-3)-r(2))/Nr))) {
			cout << "rozmieszczenie punktow siatki ulegnie zmianie" << endl;

		//Interpolacja macierzy yt do nowego rozmieszczenia punkt�w siatki oraz przypisanie nowych warto�ci w�z�om siatki
			
			int Neq=2; //Liczba r�wna� r�niczkowych cz�stkowych
			Vdoub rold(Nr);
			for (i=0; i<Nr; i++)
				rold(i)=r(i+2);
			
			for (j=0; j<kmax+1; j++) {	
				for (i=0; i<Nr; i++)
					yttemp(i)=yt(j,i);
				CSpline A1(rold,yttemp);
				for (i=0; i<Nr; i++)
					yttemp(i)=yt(j,Nr+i);
				CSpline A2(rold,yttemp);
				for (i=0; i<Nr; i++) {
					yt(j,i)=A1.Interp(rtemp(i));
					yt(j,Nr+i)=A2.Interp(rtemp(i));
				}
			}

			for (i=2; i<Mr-2; i++)
				r(i)=rtemp(i-2);
			r(0)=2*rtemp(0)-rtemp(2);
			r(1)=2*rtemp(0)-rtemp(1);
			r(Mr-2)=2*rtemp(Nr-1)-rtemp(Nr-2);
			r(Mr-1)=2*rtemp(Nr-1)-rtemp(Nr-3);
			r.display();
		}

//**********************************************************
*/


	// Reduction of integration step if error is too big.

	if (ek<1 && nstp>1) {

		if (h*(ek/Coef1)<hmin) throw("Integration step reached minimum.");

		cout << "Reducing h " << ek/Coef1 << " times , due to error of variable no " << l;
		x-=h;
		hold=h;
		h*=ek/Coef1;
		cout << "  h=" << h << endl;
		for (i=0; i<Nvar; i++) {
			y(i)=yt(0,i);
		}
		Control=1;
		goto Start1;
	}

	cout << "h=" << h << "  nstp=" << nstp << "  nk=" << nk << "  k=" << k << "  x=" << x << endl << endl;

	// Save last results to a file.

	output << nstp << " " << h << " ";
	output << k << " " << x << " ";
	for (int j=0; j<Nvar; j++) {
		output << y(j) << " ";
	}
	for (int j=2; j<Nr+2; j++) {
		output << r(j) << " ";
	}

	output << endl;

	nk++;
	nstp++;
	begin=0;
	
	// Change method tier.

	if (nk>k-1) {


		// Optimal method tier.

		kold=k;
		for (k=1; k<=kmax; k++) {

			if (k<=nstp) {
			// Calculate predictor coefficients.
				for (i=1; i<=k; i++) {
					ga(i)=1;
					for (j=0; j<=k; j++) {
						if (i!=j) {
							ga(i)*=xdif(0,j+1)/xdif(i+1,j+1);
						}
					}
				}
				ga(0)=1;
				for (i=0; i<k; i++) {
					ga(0)-=ga(i+1);
				}

				//Predictor

				for (i=0; i<Nvar; i++) {
					yP(i)=0;
					for (j=0; j<=k; j++) {
						yP(i)+=ga(j)*yt(j,i);
					}
				}

				// Calculate error.
				for (i=0; i<Nvar; i++) {
					err(i)=pow((rtol(i)*xdif(0,k+1))/(abs(y(i)-yP(i))),(1.0/k));//*(xk-xout(0))
				}
				i=err.min();
				ek1(k-1)=err(i);
			}
		}

		// Optimal method tier.
		k=ek1.max()+1;
		if (kold!=k) nk=0;
	}


	if (ek>Coef1) {
		if (ek/Coef1>ekmax) h*=ekmax;
		else h*=ek/Coef1;
		//if (h>0.5) h=0.5;
	}

	for (i=kmax; i>0; i--) {
		for (j=0; j<Nvar; j++) {
			yt(i,j)=yt(i-1,j);
		}
	}
	for (i=0; i<Nvar; i++) {
		yt(0,i)=y(i);
	}

	while (x>=xout(n)) {
		for (i=0; i<Nvar; i++) {
			yout(i,n)=0;
			for (j=0; j<=k; j++) {
				pom=1;
				for (l=0; l<=k; l++) {
					if (l!=j) pom*=(xout(n)-x+xdif(0,l))/xdif(j,l);
				}
				pom*=yt(j,i);
				yout(i,n)+=pom;
			}
		}
		if (n>=(N-1)) break;
		n++;
	}


	// Save temporary values and parameters to be able to continue later.
	ofstream param ("param.txt", ios::out | ios::trunc);
	param << nstp << " " << n << " " << k << " " << h << " " << nk << " " << x;
	param.close();

	ofstream ytsave ("yt.txt", ios::out | ios::trunc);
	for (j=0; j<kmax+1; j++) {
		for (i=0; i<Nvar; i++) ytsave << yt(j,i) << " ";
		ytsave << endl;
	}
	ytsave.close();

	ofstream youtsave ("yout.txt", ios::out | ios::trunc);
	for (j=0; j<Nvar; j++) {
		for (i=0; i<N; i++) youtsave << yout(j,i) << " ";
		youtsave << endl;
	}
	youtsave.close();

	ofstream xdifsave ("xdif.txt", ios::out | ios::trunc);
	for (j=0; j<kmax+2; j++) {
		for (i=0; i<kmax+2; i++) xdifsave << xdif(j,i) << " ";
		xdifsave << endl;
	}
	xdifsave.close();

	if (x<xk) goto Start1;
	output.close();
}