template <class D>
void Newton (D &functor, Vdoub &init) {
	int N=init.size();
	int i,j,k;
	double crit=1;
	Vdoub temp1(N), temp2(N);
	Vdoub init2(N);
	Mdoub dfdy(N,N);
	Vdoub h(N);
	const double coef=1e-15;
	const double eta=1e-5; // Iteration finish criterion.
	
	for (k=0; k<500; k++) {
		if (crit<eta) break;
		for (i=0; i<N; i++) {
			if (init(i)!=0)
				h(i)=coef*init(i);
			else
				h(i)=coef;
		}
		temp1=functor(init);
		for (i=0; i<N; i++) {
			for (j=0; j<N; j++) {
				if (i==j)
					init2(j)=init(j)+h(j);
				else
					init2(j)=init(j);
			}
			temp2=functor(init2);
			for (j=0; j<N; j++) {
				dfdy(j,i)=(temp2(j)-temp1(j))/h(j);
			}
		}
		//dfdy.display();
		LUdcmp alu(dfdy);
		for (i=0; i<N; i++)
			temp1(i)=-temp1(i);
		alu.solve(temp1,temp1);
		for (i=0; i<N; i++) {
			init(i)+=temp1(i);
			temp1(i)=abs(temp1(i));
		}
		i=temp1.max();
		temp1.display();
		init.display();/**/
		crit=temp1(i);
	}
};