#include <utils.h>
#include <ludcmp.h>
#include <qrdcmp.h>
#include <cspline.h>
#include <PDEtools.h>
#include <BGH_for_PDE.h>
#include <newton.h>
#include <roots_multidim.h>

// Integrating parameters:
const double tk = 2180.0;

// Equation parameters:
const double h00 = 1;
const double eta = 0.01;
const double R = 4;
const double G0 = 1e-04;
const double Gb = 0;
const double k0 = 3;
const double k1 = 0.6;
const double V = 0.2;
const double B = 1e-03;
const double m = 3;
const double gmw = 4e-02; //[N/m]
const double Sw = 4e-02;  //[N/m]
const double lam = 1;
const double Pe = 1000000;
const double Finf = 0;
const double Gmw = 1.23e-03; //[mol/m^2]
const double h0w=1e-03; //[m]
const double ESC=3.28567e+18; //[N*m^2/Mol^2]
const double rk = R*3/4;

// Equation constans:
const double A1=-(SQR(eta)*gmw)/(2*Sw);
const double B1=-R/(4*lam);
const double A2=(2*eta*gmw)/(R*Sw);

struct PDEs {
	Mdoub Dh;
	Mdoub DG;
	Mdoub Dp;
	int N;
	
	PDEs(int Nn) {
		N=Nn;
		Dp.resize(N,3);
	}
	Vdoub operator ()(Vdoub h, Vdoub G, Vdoub r) {
		double vri, dvridr;
		Vdoub rg(N+2);
		for (int i=0; i<N+2; i++) rg(i)=r(i+1);
		double pES=0;
		double pd=0;

		Dh=d2occ(h,r);
		DG=d2occ(G,rg);
		
		Vdoub p(N+2);
		for (int i=1; i<N+2; i++) {
			/*for (int j=1; j<N+2; j++) {
				double a1=rg(i)-rg(j);
				double a2=rg(i)+rg(j);
				double b1=Dh(i,0)+Dh(j,0);
				pES+=ESC*(h0w*SQR(Gmw*eta)/Sw)*G(i)*G(j)*(SQR(rg(j))-SQR(rg(j-1)))*b1/pow((SQR(eta*a1)+SQR(b1)),1.5);
				pES+=ESC*(h0w*SQR(Gmw*eta)/Sw)*G(i)*G(j)*(SQR(rg(j))-SQR(rg(j-1)))*b1/pow((SQR(eta*a2)+SQR(b1)),1.5);
			}*/

			if (i==1) pES=0;
			pd=(1/(2*(abs(Dh(i,1))+1.0)))*SQR(V);
			if (rg(i)==0) p(i)=A2+A1*2*Dh(i,2)+Finf+B/pow(Dh(i,0),m)-pES+pd;
			else p(i)=A2+A1*((1/rg(i))*Dh(i,1)+Dh(i,2))+Finf+B/pow(Dh(i,0),m)-pES+pd;
			pES=0;
		}
		p(0)=p(2);
		//p.display();
		Dp=d2occ(p,rg);

		Vdoub dydx(2*N);
		for (int i=0; i<N; i++) {
			if (r(i+2)==0) {
				vri=B1*2*DG(i,1);
				dvridr=B1*(2*DG(i,2)+Dh(i+1,0)*Dp(i,2));
				dydx(i)=(1/6)*pow(Dh(i+1,0),3)*Dp(i,2)-2*Dh(i+1,0)*dvridr;
				dydx(N+i)=(2/Pe)*DG(i,2)-2*DG(i,0)*dvridr;
			}
			else {
				vri=B1*(2*DG(i,1)+Dh(i+1,0)*Dp(i,1));
				dvridr=B1*(2*DG(i,2)+Dh(i+1,1)*Dp(i,1)+Dh(i+1,0)*Dp(i,2));
				dydx(i)=(pow(Dh(i+1,0),3)/12)*((1/r(i+2))*Dp(i,1)+(3/Dh(i+1,0))*Dh(i+1,1)*Dp(i,1)+Dp(i,2))-((Dh(i+1,0)/r(i+2))*vri+Dh(i+1,1)*vri+Dh(i+1,0)*dvridr);
				dydx(N+i)=(1/(Pe*r(i+2)))*(DG(i,1)+r(i+2)*DG(i,2))-((1/r(i+2))*DG(i,0)*vri+DG(i,0)*dvridr+vri*DG(i,1));
			}
		}
		dydx(N-1)=-V;
		return dydx;
	}
};

template <class D>
struct Boundary2 {

	Vdoub hin;
	Vdoub gin;
	Vdoub rh;
	int N;
	typename D &PDE;

	Boundary2(Vdoub hin1, Vdoub gin1, Vdoub rr, typename D &PDEe) : PDE(PDEe) {
		hin=hin1;
		gin=gin1;
		rh=rr;
		N=rr.size()-4;
		hin=hin1;
		gin=gin1;
	};
	Vdoub operator ()(Vdoub y) {
		Vdoub Eq(3);
		Vdoub pom(2*N);

		hin(N+2)=y(0);
		hin(N+3)=y(1);
		gin(N+1)=y(2);
	
		pom=PDE(hin, gin, rh);

		Eq(0)=PDE.Dp(N-1,0);
		Eq(1)=pom(N-1)+V;
		Eq(2)=PDE.DG(N-1,1);
		return Eq;
	}
};

template <class D>
struct System {
	double rmax;
	int N;
	double test;
	typename D &PDE;
	Vdoub init;
	int calls;
	double h;
	
	// Ordinary differential equation system.
	System(int Nn, typename D &PDEe) : N(Nn), test(-0.1), PDE(PDEe){
		calls=0;
		init.resize(3);
	};

	void operator ()(const double x, Vdoub &y, Vdoub r, Vdoub &dydx) {
		int i;
		int Nh=N+4;
		int Ng=N+2;
		Vdoub h1(Nh), G(Ng);

		// Internal values for H and G.
		for (i=2; i<Nh-2; i++) {
			h1(i)=y(i-2);
		}
		for (i=1; i<Ng-1; i++) {
			G(i)=y(N+i-1);
		}		

		// Values for i=-1, i=-2, i=N i i=N+1 from boundary conditions

		//h1(Nh-2)=h00+SQR(r(N+2))/(eta*R)-V*x;
		//h1(Nh-1)=h00+SQR(r(N+3))/(eta*R)-V*x;
		//y.display();
		//cout << "N=" << N << endl;
		//r.display();
		h1(Nh-2)=h00+(1.0/eta)*(R-pow(SQR(R)-SQR(r(N+2)),0.5))-V*x;//;
		h1(Nh-1)=h00+(1.0/eta)*(R-pow(SQR(R)-SQR(r(N+3)),0.5))-V*x;//;
		G(Ng-1)=y(2*N-2);/**/
		G(Ng-2)=y(2*N-2);
		//y(2*N-1)=y(2*N-2);
        
        if (calls==0) {
			init(0)=y(N-2);
			init(1)=y(N-1);
			init(2)=y(2*N-1);
		}

		h1(0)=y(2);
		h1(1)=y(1);
		G(0)=y(N+1);
		
		/*if (x>test) {
			init.display();
			hin.display();
			gin.display();
		}*/

		/*if (abs(x-test)>1e-30) {
			Boundary2<PDEs> Bou(h1, G, r, PDE);
			bool check;
			broydn(init,check,Bou);//Newton(Bou,init);
		}
		
		h1(Nh-2)=init(0);
		h1(Nh-1)=init(1);
		G(Ng-1)=init(2);*/
		
		dydx=PDE(h1,G,r);
		calls++;
		if (abs(x-test)>1e-30) {
			cout << "Dh(1,1)=" << PDE.Dh(1,1) << endl;
			cout << "DG(0,1)=" << PDE.DG(0,1) << endl;
			cout << "Dp(0,1)=" << PDE.Dp(0,1) << endl;
			cout << "dhdt(N-1)=" << dydx(N-1) << endl;
			cout << "DG(N-1,1)=" << PDE.DG(N-1,1) << endl;
			cout << "DG(N-1,0)=" << PDE.DG(N-1,0) << endl;
			cout << "p(0)=" << PDE.Dp(0,0) << endl;
			cout << "p(N-1)=" << PDE.Dp(N-1,0) << endl;
			//for (i=N-5; i<N; i++) cout << "p(" << i << ")=" << PDE.Dp(i,0) << " ";
			cout << endl;
			test=x;
		}

	}
	Mdoub jacobian(double h, Vdoub y) {
		Mdoub dfdy(2*N,2*N);
		return dfdy;
	}
};


void main() {

	cout << "A1=" << A1 << endl;
	cout << "B1=" << B1 << endl;
	cout << "A2=" << A2 << endl;
	cout << "tk=" << tk << endl;
	cout << "rk=" << rk << endl;
	int N=300; // Number of lines.
	int Nt=100; // Number of points for each line.

	// Variable definitions and initial calculations.
	Mdoub H(N,Nt), G(N,Nt);
	Vdoub r0(N+4);
	Vdoub t(Nt);
	for (int i=0; i<N+4; i++)
		r0(i)=(i-2)*rk/(N-1); //(equally spaced points)
		//r0(i)=SQR(i-2)*rk/(SQR(N-1));//(parabolic, lowest step for y=0)
		//-(rk/(SQR(N-1)))*SQR(i-2)+(2*rk/(N-1))*(i-2); //(parabolic, lowest step for y=rk)

	//r0.display();

	// Initial conditions:

	for(int i=0; i<N; i++) {
		//H(i,0)=h00+SQR(r0(i+2))/(eta*R);  Profil paraboliczny
		//H(i,0)=h00; // Profil p�aski
		H(i,0)=h00+(1.0/eta)*(R-pow(SQR(R)-SQR(r0(i+2)),0.5)); //Profil okr�gu
		G(i,0)=G0;
		//G(i,0)=(1-Gb)*((1-tanh((r0(i+2)-k0)/k1))/(1-tanh(-k0/k1)))+Gb; Profil odwr�cony sigmoidalny
	}
	
	//double h=rk/(N-1); //cout << "h=" << h << " ";

	PDEs PDE(N);

	System<PDEs> F1(N, PDE);
		
	Vdoub ystart(2*N);
	for (int i=0; i<N; i++)
		ystart(i)=H(i,0);
	for (int i=0; i<N; i++)
		ystart(N+i)=G(i,0);

	// BGH method parameters.
	const double h1=1e-3, hmin=1e-12, x1=0.0, x2=tk;
	Vdoub rtol(2*N);
	for (int i=0; i<N; i++) {
		rtol(i)=1e-03;
	}
	for (int i=0; i<N; i++) {
		rtol(N+i)=1e-03;
	}


	BGH<System<PDEs>> Equ1(h1,hmin,Nt,rtol,x1,x2,ystart,r0,F1,0,0);
	Equ1.integrate();
}
