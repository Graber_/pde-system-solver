template <class T>
void lnsrch(Vdoub &xold, const double fold, Vdoub &g, Vdoub &p,
Vdoub &x, double &f, const double stpmax, bool &check, T &func) {
/*Given an n-dimensional point xold(0..n-1), the value of the function and gradient there, fold
and g(0..n-1), and a direction p(0..n-1), ﬁnds a new point x(0..n-1) along the direction
p from xold where the function or functor func has decreased “suﬃciently.” The new function
value is returned in f. stpmax is an input quantity that limits the length of the steps so that you
do not try to evaluate the function in regions where it is undeﬁned or subject to overﬂow. p is
usually the Newton direction. The output quantity check is false on a normal exit. It is true
when x is too close to xold. In a minimization algorithm, this usually signals convergence and
can be ignored. However, in a zero-ﬁnding algorithm the calling program should check whether
the convergence is spurious.*/
	const double ALF=1.0e-4, TOLX=numeric_limits<double>::epsilon();
	//ALF ensures suﬃcient decrease in function value; TOLX is the convergence criterion on x.
	double a,alam,alam2=0.0,alamin,b,disc,f2=0.0;
	double rhs1,rhs2,slope=0.0,sum=0.0,temp,test,tmplam;
	int i,n=xold.size();
	check=false;
	for (i=0;i<n;i++) sum += p(i)*p(i);
	sum=sqrt(sum);
	if (sum > stpmax)
		for (i=0;i<n;i++)
			p(i) *= stpmax/sum; //Scale if attempted step is too big.
	for (i=0;i<n;i++)
		slope += g(i)*p(i);
	if (slope >= 0.0) {
		cout << "Roundoff problem in lnsrch." << endl;
		goto end;
	}
	test=0.0; //Compute min.
	for (i=0;i<n;i++) {
		temp=abs(p(i))/MAX(abs(xold(i)),1.0);
		if (temp > test) test=temp;
	}
	alamin=TOLX/test;
	alam=1.0; //Always try full Newton step ﬁrst.
	for (;;) { //Start of iteration loop.
		for (i=0;i<n;i++) x(i)=xold(i)+alam*p(i);
		f=func(x);
		if (alam < alamin) { //Convergence on x. For zero ﬁnding,	the calling program should verify the convergence.
			for (i=0;i<n;i++) x(i)=xold(i);
			check=true;
			return;
		} else if (f <= fold+ALF*alam*slope) return; //Suﬃcient function decrease.
		else { //Backtrack.
			if (alam == 1.0)
				tmplam = -slope/(2.0*(f-fold-slope)); //First time.
			else { //Subsequent backtracks.
				rhs1=f-fold-alam*slope;
				rhs2=f2-fold-alam2*slope;
				a=(rhs1/(alam*alam)-rhs2/(alam2*alam2))/(alam-alam2);
				b=(-alam2*rhs1/(alam*alam)+alam*rhs2/(alam2*alam2))/(alam-alam2);
				if (a == 0.0) tmplam = -slope/(2.0*b);
				else {
					disc=b*b-3.0*a*slope;
					if (disc < 0.0) tmplam=0.5*alam;
					else if (b <= 0.0) tmplam=(-b+sqrt(disc))/(3.0*a);
					else tmplam=-slope/(b+sqrt(disc));
				}
				if (tmplam>0.5*alam)
					tmplam=0.5*alam; //l<=0.5l1.
			}
		}
		alam2=alam;
		f2 = f;
		alam=MAX(tmplam,0.1*alam); //l>=0.1l1.
	} //Try again.
end:;
}
template <class T>
void newt(Vdoub &x, bool &check, T &vecfunc) {
/*Given an initial guess x(0..n-1) for a root in n dimensions, ﬁnd the root by a globally convergent
Newton’s method. The vector of functions to be zeroed, called fvec(0..n-1) in the routine
below, is returned by the user-supplied function or functor vecfunc (see text). The output
quantity check is false on a normal return and true if the routine has converged to a local
minimum of the function fmin deﬁned below. In this case try restarting from a diﬀerent initial
guess.*/
	const int MAXITS=1500;
	const double TOLF=1.0e-15,TOLMIN=1.0e-15,STPMX=100.0;
	const double TOLX=numeric_limits<double>::epsilon();
/*Here MAXITS is the maximum number of iterations; TOLF sets the convergence criterion on
function values; TOLMIN sets the criterion for deciding whether spurious convergence to a
minimum of fmin has occurred; STPMX is the scaled maximum step length allowed in line
searches; and TOLX is the convergence criterion on ıx.*/
	int i,j,its,n=x.size();
	double den,f,fold,stpmax,sum,temp,test;
	Vdoub g(n),p(n),xold(n);
	Mdoub fjac(n,n);
	NRfmin<T> fmin(vecfunc); //Set up NRfmin object.
	NRfdjac<T> fdjac(vecfunc); //Set up NRfdjac object.
	Vdoub &fvec=fmin.fvec; //Make an alias to simplify coding.
	f=fmin(x); //fvec is also computed by this call.
	test=0.0; //Test for initial guess being a root. Use more stringent test than simply TOLF. 
	for (i=0;i<n;i++)
		if (abs(fvec(i)) > test) test=abs(fvec(i));
	if (test < 0.01*TOLF) {
		check=false;
		return;
	}
	sum=0.0;
	for (i=0;i<n;i++) sum += SQR(x(i)); //Calculate stpmax for line searches.
	stpmax=STPMX*MAX(sqrt(sum),double(n));
	for (its=0;its<MAXITS;its++) { //Start of iteration loop.
		fjac=fdjac(x,fvec);
		//If analytic Jacobian is available, you can replace the struct NRfdjac below with your own struct.
		for (i=0;i<n;i++) { //Compute rf for the line search.
			sum=0.0;
			for (j=0;j<n;j++) sum += fjac(j,i)*fvec(j);
			g(i)=sum;
		}
		for (i=0;i<n;i++) xold(i)=x(i); //Store x,
		fold=f; //and f .
		for (i=0;i<n;i++) p(i) = -fvec(i); //Right-hand side for linear equations.
		LUdcmp alu(fjac); //Solve linear equations by LU decomposition.
		alu.solve(p,p);
		lnsrch(xold,fold,g,p,x,f,stpmax,check,fmin); //lnsrch returns new x and f . It also calculates fvec at the new x when it calls fmin.
		test=0.0; //Test for convergence on function values.
		for (i=0;i<n;i++)
			if (abs(fvec(i)) > test) test=abs(fvec(i));
		if (test < TOLF) {
			check=false;
			return;
		}
		if (check) { //Check for gradient of f zero, i.e., spurious convergence.
			test=0.0;
			den=MAX(f,0.5*n);
			for (i=0;i<n;i++) {
				temp=abs(g(i))*MAX(abs(x(i)),1.0)/den;
				if (temp > test) test=temp;
			}
			check=(test < TOLMIN);
			return;
		}
		test=0.0; //Test for convergence on ıx.
		for (i=0;i<n;i++) {
			temp=(abs(x(i)-xold(i)))/MAX(abs(x(i)),1.0);
			if (temp > test) test=temp;
		}
		if (test < TOLX)
			return;
	}
	cout << "MAXITS exceeded in newt" << endl;
}
template <class T>
struct NRfdjac {
//Computes forward-diﬀerence approximation to Jacobian.
	const double EPS; //Set to approximate square root of the machine precision.
	T &func;
	NRfdjac(T &funcc) : EPS(1.0e-8),func(funcc) {}
	//Initialize with user-supplied function or functor that returns the vector of functions to be zeroed.
	Mdoub operator() (Vdoub &x, Vdoub &fvec) {
/*Returns the Jacobian array df(0..n-1,0..n-1). On input, x(0..n-1) is the point at
which the Jacobian is to be evaluated and fvec(0..n-1) is the vector of function values
at the point.*/
		int n=x.size();
		Mdoub df(n,n);
		Vdoub xh=x;
		for (int j=0;j<n;j++) {
			double temp=xh(j);
			double h=EPS*abs(temp);
			if (h == 0.0) h=EPS;
			xh(j)=temp+h; //Trick to reduce ﬁnite-precision error.
			h=xh(j)-temp;
			Vdoub f=func(xh);
			xh(j)=temp;
			for (int i=0;i<n;i++) //Forward diﬀerence formula.
				df(i,j)=(f(i)-fvec(i))/h;
		}
		return df;
	}
};
template <class T>
struct NRfmin {
//Returns f D 1 2F  F. Also stores value of F in fvec.
	Vdoub fvec;
	T &func;
	int n;
	NRfmin(T &funcc) : func(funcc){}
	//Initialize with user-supplied function or functor that returns the vector of functions to be zeroed.
	double operator() (Vdoub &x) {	//Returns f at x, and stores F.x/ in fvec.
		n=x.size();
		double sum=0;
		fvec=func(x);
		for (int i=0;i<n;i++) sum += SQR(fvec(i));
		return 0.5*sum;
	}
};
template <class T>
void broydn(Vdoub &x, bool &check, T &vecfunc) {
/*Given an initial guess x(0..n-1) for a root in n dimensions, ﬁnd the root by Broyden’s
method embedded in a globally convergent strategy. The vector of functions to be zeroed,
called fvec(0..n-1) in the routine below, is returned by the user-supplied function or functor
vecfunc. The routines NRfdjac and NRfmin from newt are used. The output quantity check
is false on a normal return and true if the routine has converged to a local minimum of the
function fmin or if Broyden’s method can make no further progress. In this case try restarting
from a diﬀerent initial guess.*/
	const int MAXITS=500;
	const double EPS=numeric_limits<double>::epsilon();
	const double TOLF=1.0e-12, TOLX=EPS, STPMX=100.0, TOLMIN=1.0e-12;
/*Here MAXITS is the maximum number of iterations; EPS is the machine precision; TOLF
is the convergence criterion on function values; TOLX is the convergence criterion on ıx;
STPMX is the scaled maximum step length allowed in line searches; and TOLMIN is used to
decide whether spurious convergence to a minimum of fmin has occurred.*/
	bool restrt,skip;
	int i,its,j,n=x.size();
	double den,f,fold,stpmax,sum,temp,test;
	Vdoub fvcold(n),g(n),p(n),s(n),t(n),w(n),xold(n);
	QRdcmp *qr;
	NRfmin<T> fmin(vecfunc); //Set up NRfmin object.
	NRfdjac<T> fdjac(vecfunc); //Set up NRfdjac object.
	Vdoub &fvec=fmin.fvec; //Make an alias to simplify coding.
	f=fmin(x); //The vector fvec is also computed by this call.
	test=0.0;
	for (i=0;i<n;i++) //Test for initial guess being a root. Use more stringent test than simply TOLF.
		if (abs(fvec(i)) > test) test=abs(fvec(i));
	if (test < 0.01*TOLF) {
		check=false;
		return;
	}
	for (sum=0.0,i=0;i<n;i++) sum += SQR(x(i)); //Calculate stpmax for line searches.
	stpmax=STPMX*MAX(sqrt(sum),double(n));
	restrt=true; //Ensure initial Jacobian gets computed.
	for (its=1;its<=MAXITS;its++) { //Start of iteration loop.
		if (restrt) { //Initialize or reinitialize Jacobian and QR decompose it.
			qr=new QRdcmp(fdjac(x,fvec));
			if (qr->sing) {
				Mdoub one(n,n,0.0);
				for (i=0;i<n;i++) one(i,i)=1.0;
				delete qr;
				qr=new QRdcmp(one);
			}
		} else { //Carry out Broyden update.
			for (i=0;i<n;i++) s(i)=x(i)-xold(i); //s D ıx.
				for (i=0;i<n;i++) { //t D R  s.
					for (sum=0.0,j=i;j<n;j++) sum += qr->r(i,j)*s(j);
					t(i)=sum;
				}
				skip=true;
				for (i=0;i<n;i++) { //w D ıF  B  s.
					for (sum=0.0,j=0;j<n;j++) sum += qr->qt(j,i)*t(j);
					w(i)=fvec(i)-fvcold(i)-sum;
					if (abs(w(i)) >= EPS*(abs(fvec(i))+abs(fvcold(i)))) skip=false; //Don’t update with noisy components of w.
					else w(i)=0.0;
				}
				if (!skip) {
					qr->qtmult(w,t); //t D QT  w.
					for (den=0.0,i=0;i<n;i++) den += SQR(s(i));
					for (i=0;i<n;i++) s(i) /= den; //Store s=.s  s/ in s.
					qr->update(t,s); //Update R and QT .
					if (qr->sing) {
						cout << "singular update in broydn" << endl;
						goto end;
					}
				}
		}
		qr->qtmult(fvec,p);
		for (i=0;i<n;i++) //Right-hand side for linear equations is QT  F.
			p(i) = -p(i);
		for (i=n-1;i>=0;i--) { //Compute rf 	 .QR/T F for the line search.
			for (sum=0.0,j=0;j<=i;j++) sum -= qr->r(j,i)*p(j);
			g(i)=sum;
		}
		for (i=0;i<n;i++) { //Store x and F.
			xold(i)=x(i);
			fvcold(i)=fvec(i);
		}
		fold=f; //Store f .
		qr->rsolve(p,p); //Solve linear equations.
		lnsrch(xold,fold,g,p,x,f,stpmax,check,fmin); //lnsrch returns new x and f . It also calculates fvec at the new x when it calls fmin.
		test=0.0; //Test for convergence on function values.
		for (i=0;i<n;i++)
			if (abs(fvec(i)) > test) test=abs(fvec(i));
		if (test < TOLF) {
			check=false;
			delete qr;
			return;
		}
		if (check) { //True if line search failed to ﬁnd a new x.
			if (restrt) { //Failure; already tried reinitializing the Jacobian.
				delete qr;
				return;
			} else {
				test=0.0; //Check for gradient of f zero, i.e., spurious convergence.
				den=MAX(f,0.5*n);
				for (i=0;i<n;i++) {
					temp=abs(g(i))*MAX(abs(x(i)),1.0)/den;
					if (temp > test) test=temp;
				}
				if (test < TOLMIN) {
					delete qr;
					return;
				}
				else restrt=true; //Try reinitializing the Jacobian.
			}
		} else { //Successful step; will use Broyden update for next step.
			restrt=false;
			test=0.0; //Test for convergence on ıx.
			for (i=0;i<n;i++) {
				temp=(abs(x(i)-xold(i)))/MAX(abs(x(i)),1.0);
				if (temp > test) test=temp;
			}
			if (test < TOLX) {
				delete qr;
				return;
			}
		}
	}
	cout << "MAXITS exceeded in broydn" << endl;
end:;
}
