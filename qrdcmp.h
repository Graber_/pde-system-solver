struct QRdcmp {
//Object for QR decomposition of a matrix A, and related functions.
	int n;
	Mdoub qt, r;											//Stored Q^T and R.
	bool sing;												//Indicates whether A is singular.
	QRdcmp(Mdoub &a);									//Constructor from A.
	void solve(Vdoub &b, Vdoub &x);					//Solve A * x D b for x.
	void qtmult(Vdoub &b, Vdoub &x);				//Multiply Q^T * b D x.
	void rsolve(Vdoub &b, Vdoub &x);				//Solve R * x D b for x.
	void update(Vdoub &u, Vdoub &v);				//See next subsection.
	void rotate(const int i, const double a, const double b);	//Used by update.
};
//As usual, the constructor performs the actual decomposition:

QRdcmp::QRdcmp(Mdoub &a)
	: n(a.nrows()), qt(n,n), r(a), sing(false) {
/*Construct the QR decomposition of a[0..n-1][0..n-1]. The upper triangular matrix R and
the transpose of the orthogonal matrix Q are stored. sing is set to true if a singularity is
encountered during the decomposition, but the decomposition is still completed in this case;
otherwise it is set to false.*/
	int i,j,k;
	Vdoub c(n), d(n);
	double scale,sigma,sum,tau;
	for (k=0;k<n-1;k++) {
		scale=0.0;
		for (i=k;i<n;i++) scale=MAX(scale,abs(r(i,k)));
		if (scale == 0.0) { //Singular case.
			sing=true;
			c(k)=d(k)=0.0;
		} else { //Form Qk and Qk * A.
			for (i=k;i<n;i++) r(i,k) /= scale;
			for (sum=0.0,i=k;i<n;i++) sum += SQR(r(i,k));
			sigma=SIGN(sqrt(sum),r(k,k));
			r(k,k) += sigma;
			c(k)=sigma*r(k,k);
			d(k) = -scale*sigma;
			for (j=k+1;j<n;j++) {
				for (sum=0.0,i=k;i<n;i++) sum += r(i,k)*r(i,j);
				tau=sum/c(k);
				for (i=k;i<n;i++) r(i,j) -= tau*r(i,k);
			}
		}
	}
	d(n-1)=r(n-1,n-1);
	if (d(n-1) == 0.0) sing=true;
	for (i=0;i<n;i++) { //Form QT explicitly.
		for (j=0;j<n;j++) qt(i,j)=0.0;
		qt(i,i)=1.0;
	}
	for (k=0;k<n-1;k++) {
		if (c(k) != 0.0) {
			for (j=0;j<n;j++) {
				sum=0.0;
				for (i=k;i<n;i++)
					sum += r(i,k)*qt(i,j);
				sum /= c(k);
				for (i=k;i<n;i++)
					qt(i,j) -= sum*r(i,k);
			}
		}
	}
	for (i=0;i<n;i++) { //Form R explicitly.
		r(i,i)=d(i);
		for (j=0;j<i;j++) r(i,j)=0.0;
	}
}
/*The next set of member functions is used to solve linear systems. In many applications
only the part (2.10.4) of the algorithm is needed, so we put in separate routines the multiplication
QT 	 b and the backsubstitution on R.*/
void QRdcmp::solve(Vdoub &b, Vdoub &x) {
/*Solve the set of n linear equations A x D b. b[0..n-1] is input as the right-hand side vector,
and x[0..n-1] is returned as the solution vector.*/
	qtmult(b,x); //Form QT * b.
	rsolve(x,x); //Solve R * x D Q^T * b.
}
void QRdcmp::qtmult(Vdoub &b, Vdoub &x) {
//Multiply QT * b and put the result in x. Since Q is orthogonal, this is equivalent to solving Q * x D b for x.
	int i,j;
	double sum;
	for (i=0;i<n;i++) {
		sum = 0.;
		for (j=0;j<n;j++) sum += qt(i,j)*b(j);
		x(i) = sum;
	}
}
void QRdcmp::rsolve(Vdoub &b, Vdoub &x) {
/*Solve the triangular set of n linear equations R * x D b. b[0..n-1] is input as the right-hand
side vector, and x[0..n-1] is returned as the solution vector.*/
	int i,j;
	double sum;
	if (sing) throw("attempting solve in a singular QR");
	for (i=n-1;i>=0;i--) {
		sum=b(i);
		for (j=i+1;j<n;j++) sum -= r(i,j)*x(j);
		x(i)=sum/r(i,i);
	}
}
void QRdcmp::update(Vdoub &u, Vdoub &v) { //qrdcmp.h
/*Starting from the stored QR decomposition A D Q*R, update it to be the QR decomposition
of the matrix Q* .RCu*v/. Input quantities are u[0..n-1], and v[0..n-1].*/
	int i,k;
	Vdoub w(u);
	for (k=n-1;k>=0;k--) //Find largest k such that u[k] != 0.
		if (w(k) != 0.0) break;
	if (k < 0) k=0;
	for (i=k-1;i>=0;i--) { //Transform RCu˝v to upper Hessenberg.
		rotate(i,w(i),-w(i+1));
		if (w(i) == 0.0)
			w(i)=abs(w(i+1));
		else if (abs(w(i)) > abs(w(i+1)))
			w(i)=abs(w(i))*sqrt(1.0+SQR(w(i+1)/w(i)));
		else w(i)=abs(w(i+1))*sqrt(1.0+SQR(w(i)/w(i+1)));
	}
	for (i=0;i<n;i++) r(0,i) += w(0)*v(i);
	for (i=0;i<k;i++) //Transform upper Hessenberg matrix to upper triangular.
		rotate(i,r(i,i),-r(i+1,i));
	for (i=0;i<n;i++)
		if (r(i,i) == 0.0) sing=true;
}
void QRdcmp::rotate(const int i, const double a, const double b)
/*Utility used by update. Given matrices r[0..n-1][0..n-1] and qt[0..n-1][0..n-1], carry
out a Jacobi rotation on rows i and i C 1 of each matrix. a and b are the parameters of the
rotation: cos t D a=pa2 Cb2, sin t D b=pa2 Cb2.*/
{
	int j;
	double c,fact,s,w,y;
	if (a == 0.0) { //Avoid unnecessary overﬂow or underﬂow.
		c=0.0;
	s=(b >= 0.0 ? 1.0 : -1.0);
	} else if (abs(a) > abs(b)) {
		fact=b/a;
		c=SIGN(1.0/sqrt(1.0+(fact*fact)),a);
		s=fact*c;
	} else {
		fact=a/b;
		s=SIGN(1.0/sqrt(1.0+(fact*fact)),b);
		c=fact*s;
	}
	for (j=i;j<n;j++) { //Premultiply r by Jacobi rotation.
		y=r(i,j);
		w=r(i+1,j);
		r(i,j)=c*y-s*w;
		r(i+1,j)=s*y+c*w;
	}
	for (j=0;j<n;j++) { //Premultiply qt by Jacobi rotation.
		y=qt(i,j);
		w=qt(i+1,j);
		qt(i,j)=c*y-s*w;
		qt(i+1,j)=s*y+c*w;
	}
}