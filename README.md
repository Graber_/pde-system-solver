# README #

Partial differential equation system solver using BGH (Brayton, Gustavson, Hatchel) method for ODE integration.

Implementation in C++.

# NOTE #

This is my first project, started in 2007, I have placed it online so maybe someone will find it usefull as it handles unstable PDE systems well, but also for history keeping reasons.

It contains code probably lacking many good practices (especially goto statements) and can be optimised a lot.