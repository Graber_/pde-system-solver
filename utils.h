#include <fstream>
#include <cmath>
#include <complex>
#include <iostream>
#include <iomanip>
#include <vector>
#include <limits>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <fcntl.h>
#include <string.h>
#include <ctype.h>

using namespace std;

// exception handling

#ifndef _USENRERRORCLASS_
#define throw(message) \
{printf("ERROR: %s\n     in file %s at line %d\n", message,__FILE__,__LINE__); throw(1);}
#else
struct NRerror {
	char *message;
	char *file;
	int line;
	NRerror(char *m, char *f, int l) : message(m), file(f), line(l) {}
};
#define throw(message) throw(NRerror(message,__FILE__,__LINE__));
void NRcatch(NRerror err) {
	printf("ERROR: %s\n     in file %s at line %d\n",
		err.message, err.file, err.line);
	exit(1);
}
#endif

// usage example:
//
//	try {
//		somebadroutine();
//	}
//	catch(NRerror s) {NRcatch(s);}
//
// (You can of course substitute any other catch body for NRcatch(s).)

template <class T>
class MVec {
private:
	T* w;
	int N;
public:
	MVec();
	explicit MVec(int n);
	MVec(int n, const T &a);
	MVec(int n, const T *a);
	MVec(const MVec &rhs);
	MVec & operator=(const MVec &rhs);
	MVec & operator=(const T &a);
	typedef T value_type;
	inline T& operator () (const int i);
	inline const T& operator () (const int i) const;
	inline int size() const;
	void resize(int newn);
	void assign(int newn, const T &a);
	void display();
	int min();
	int max();
	~MVec();
};

template <class T>
MVec<T>::MVec() : N(0), w(NULL) {}

template <class T>
MVec<T>::MVec(int n) : N(n), w(n>0 ? new T[n] : NULL) {
	for (int i=0; i<N; i++) w[i] = 0;
}

template <class T>
MVec<T>::MVec(int n, const T& a) : N(n), w(n>0 ? new T[n] : NULL) {
	for(int i=0; i<n; i++) w[i]=a;
}

template <class T>
MVec<T>::MVec(int n, const T *a) : N(n), w(n>0 ? new T[n] : NULL) {
	for(int i=0; i<n; i++) w[i]= *a++;
}

template <class T>
MVec<T>::MVec(const MVec<T> &rhs) : N(rhs.N), w(rhs.N>0 ? new T[rhs.N] : NULL)
{
	for(int i=0; i<N; i++) w[i]=rhs.w[i];
}

template <class T>
MVec<T> & MVec<T>::operator =(const MVec<T> &rhs) {
	if (this != &rhs)
	{
		if (N != rhs.N) {
			if (w != NULL) delete[] (w);
			N=rhs.N;
			w= N>0 ? new T[N] : NULL;
		}
		for (int i=0; i<N; i++)
			w[i]=rhs(i);
	}
	return *this;
}

template <class T>
MVec<T> & MVec<T>::operator =(const T &a) {
	N=1;
	w=new T[1];
	w[0]=a;
	return *this;
}

template <class T>
inline T & MVec<T>::operator ()(const int i) {
	if (i<0 || i>=N) throw("Nieprawidlowa wartosc indeksu");
	return w[i];
}

template <class T>
inline const T & MVec<T>::operator ()(const int i) const {
	if (i<0 || i>=N) throw("Nieprawidlowa wartosc indeksu");
	return w[i];
}

template <class T>
inline int MVec<T>::size() const {
	return N;
}

template <class T>
void MVec<T>::resize(int newn) {
	if (newn != N) {
		if (w != NULL) delete[] (w);
		N = newn;
		w = N > 0 ? new T[N] : NULL;
	}
	for (int i=0; i<N; i++) {
		w[i]=0;
	}
}

template <class T>
void MVec<T>::assign(int newn, const T &a) {
	if (newn != N) {
		if (w != NULL) delete[] (w);
		N = newn;
		w = N > 0 ? new T[N] : NULL;
	}
	for (int i=0; i<N; i++) w[i] = a;
}

template <class T>
MVec<T>::~MVec() {
	if (w != NULL) delete[] (w);
}

template <class T>
void MVec<T>::display() {
	cout << "liczba elementow:" << N << endl;
	for (int i=0; i<N; i++) cout << w[i] << " ";
	cout << endl << endl;
}

template <class T>
int MVec<T>::min() {
	T temp=w[0];
	int min=0;
	for (int i=1; i<N; i++) {
		if (w[i]<temp) {
			temp=w[i];
			min=i;
		}
	}
	return min;
}

template <class T>
int MVec<T>::max() {
	T temp=w[0];
	int max=0;
	for (int i=1; i<N; i++) {
		if (w[i]>temp) {
			temp=w[i];
			max=i;
		}
	}
	return max;
}



//End of Vector class

template <class T>
class MMat {
private:
	T * w;
	int N; //Liczba wierszy
	int	M; //Liczba kolumn
public:
	MMat();
	MMat(int n, int m);
	MMat(int n, int m, const T &a);
	MMat(int n, int m, const T *a);
	MMat(const MMat &rhs);
	MMat & operator=(const MMat &rhs);
	typedef T value_type;
	inline T& operator () (int i, int j);
	inline const T& operator () (int i, int j) const;
	inline int nrows() const;
	inline int ncols() const;
	void resize(int newn, int newm);
	void assign(int newn, int newm, const T &a);
	void display();
	~MMat();
};

template <class T>
MMat<T>::MMat() : N(0), M(0), w(NULL) {}

template <class T>
MMat<T>::MMat(int n, int m) : N(n), M(m), w(n>0 ? new T[n*m] : NULL) {
	for (int i=0; i<N*M; i++)
		w[i]=0;
	
}

template <class T>
MMat<T>::MMat(int n, int m, const T &a) : N(n), M(m), w(n>0 ? new T[n*m] : NULL) {
	for (int i=0; i<(N*M); i++) w[i]=a;
}

template <class T>
MMat<T>::MMat(int n, int m, const T *a) : N(n), M(m), w(n>0 ? new T[n*m] : NULL) {
	for (int i=0; i<(N*M); i++) w[i] = *a++;
}

template <class T>
MMat<T>::MMat(const MMat &rhs) : N(rhs.N), M(rhs.M), w(rhs.N>0 ? new T[rhs.N*rhs.M] : NULL) {
	for (int i=0; i<(N*M); i++) w[i] = rhs.w[i];
}

template <class T>
MMat<T> & MMat<T>::operator =(const MMat<T> &rhs) {
	if (this != &rhs) {
		if (N != rhs.N || M != rhs.M) {
			if (w != NULL) delete[] (w);
			N = rhs.N;
			M = rhs.M;
			w = N>0 ? new T[N*M] : NULL;
		}
		for (int i=0; i<(N*M); i++) w[i] = rhs.w[i];
	}
	return *this;
}

template <class T>
inline T& MMat<T>::operator ()(int n, int m) {
	if ((n<0||n>=N)||(m<0||m>=M)) throw("Nieprawidlowa wartosc indeksu");
	return w[n*(M)+m];
}

template <class T>
inline const T& MMat<T>::operator ()(int n, int m) const {
	if ((n<0||n>=N)||(m<0||m>=M)) throw("Nieprawidlowa wartosc indeksu");
	return w[n*(M)+m];
}

template <class T>
inline int MMat<T>::nrows() const {
	return N;
}

template <class T>
inline int MMat<T>::ncols() const {
	return M;
}

template <class T>
void MMat<T>::resize(int newn, int newm) {
	if (newn != N || newm != M) {
		if (w != NULL) delete[] w;
		N = newn;
		M = newm;
		w = new T[N*M];
	}
	for (int i=0; i<N*M; i++) {
	w[i]=0;
	}
}

template <class T>
void MMat<T>::assign(int newn, int newm, const T& a) {
	if (newn != N || newm != M) {
		if (w != NULL) delete[] w;
		N = newn;
		M = newm;
		w = new T[N*M];
	}
	for (int i=0; i<(N*M); i++) w[i] = a;
}

template <class T>
void MMat<T>::display() {
	cout << "liczba wierszy=" << N << endl;
	cout << "liczba kolumn=" << M << endl;
	for (int j=0; j<N; j++) {
		for (int i=0; i<M; i++) {
			cout << w[j*(M)+i] << " ";
		}
		cout << endl;
	}
	cout << endl;
}

template <class T>
MMat<T>::~MMat() {
	if (w != NULL) delete[] (w);
}

//definicje typ�w wektor�w i macierzy
typedef MVec<double> Vdoub;
typedef MVec<string> Vstring;
typedef MVec<int> Vint;

typedef MMat<double> Mdoub;
typedef MMat<int> Mint;

template<class T>
inline T SIGN(const T &a, const T &b)
	{return b >= 0 ? (a >= 0 ? a : -a) : (a >= 0 ? -a : a);}

inline float SIGN(const float &a, const double &b)
	{return b >= 0 ? (a >= 0 ? a : -a) : (a >= 0 ? -a : a);}

inline float SIGN(const double &a, const float &b)
	{return (float)(b >= 0 ? (a >= 0 ? a : -a) : (a >= 0 ? -a : a));}

//Additional simple functions

template<class T>
inline T SQR(const T a) {return a*a;}

template<class T>
inline const T MAX(const T &a, const T &b)
        {return b > a ? (b) : (a);}

template<class T>
inline const T MIN(const T &a, const T &b)
        {return b < a ? (b) : (a);}