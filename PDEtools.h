Mdoub d2occ(Vdoub y, Vdoub x) {
	int Ny=y.size();
	int N=Ny-2;
	double h=x(1)-x(0);
	int i;
	Mdoub derivs(N,3);
	for (i=0; i<N; i++) {
		derivs(i,0)=y(i+1);
		derivs(i,1)=(y(i+2)-y(i))/(2*h);
		derivs(i,2)=(y(i+2)-2*y(i+1)+y(i))/SQR(h);
	}
	return derivs;
}

Mdoub d24occ(Vdoub y, Vdoub x) {
	int Ny=y.size();
	int N=Ny-2;
	double h=x(1)-x(0);
	int i;
	Mdoub derivs(N,3);
	for (i=0; i<N; i++) {
		derivs(i,0)=y(i+1);
		if (i==0 || i==N-1) {
		derivs(0,1)=(y(2)-y(0))/(2*h);
		derivs(0,2)=(y(2)-2*y(1)+y(0))/SQR(h);
		derivs(N-1,1)=(y(Ny-1)-y(Ny-2))/(2*h);
		derivs(N-1,2)=(y(Ny-1)-2*y(Ny-2)+y(Ny-3))/SQR(h);
		}
		else {
			derivs(i,1)=(-y(i+3)+8*y(i+2)-8*y(i)+y(i-1))/(12*h);
			derivs(i,2)=(-y(i+3)+16*y(i+2)-30*y(i+1)+16*y(i)-y(i-1))/(12*SQR(h));
		}
	}
	return derivs;
}

Mdoub d2ocv(Vdoub y, Vdoub x) {
	int Ny=y.size();
	int N=Ny-2;
	int i;
	Mdoub derivs(N,3);
	Mdoub A(2,2);
	Vdoub right(2);
	for (i=0; i<N; i++) {
		A(0,0)=x(i+2)-x(i+1);
		A(1,0)=x(i)-x(i+1);
		A(0,1)=0.5*SQR(A(0,0));
		A(1,1)=0.5*SQR(A(1,0));
		LUdcmp lu(A);
		right(0)=y(i+2)-y(i+1);
		right(1)=y(i)-y(i+1);
		lu.solve(right,right);
		derivs(i,0)=y(i+1);
		derivs(i,1)=right(0);
		derivs(i,2)=right(1);
	}
	return derivs;
}

Mdoub d4occ(Vdoub y, Vdoub x) {
	int N=y.size()-4;
	double h=x(1)-x(0);
	int i;
	Mdoub derivs(N,5);
	for (i=0; i<N; i++) {
		derivs(i,0)=y(i+2);
		derivs(i,1)=(-y(i+4)+8*y(i+3)-8*y(i+1)+y(i))/(12*h);
		derivs(i,2)=(-y(i+4)+16*y(i+3)-30*y(i+2)+16*y(i+1)-y(i))/(12*SQR(h));
		derivs(i,3)=(y(i+4)-2*y(i+3)+2*y(i+1)-y(i))/(2*pow(h,3));
		derivs(i,4)=(y(i+4)-4*y(i+3)+6*y(i+2)-4*y(i+1)+y(i))/pow(h,4);
	}
	return derivs;
}

Mdoub d4ocv(Vdoub y, Vdoub x) {
	int N=y.size()-4;
	int i,j,k;
	Mdoub derivs(N,5);
	Mdoub A(4,4);
	Vdoub right(4);
	for (i=0; i<N; i++) {
		A(0,0)=x(i+4)-x(i+2);
		A(1,0)=x(i+3)-x(i+2);
		A(2,0)=x(i+1)-x(i+2);
		A(3,0)=x(i)-x(i+2);
		for (j=0; j<4; j++) {
			for (k=1; k<4; k++) {
				A(j,k)=(A(j,0)*A(j,k-1))/(k+1);
			}
		}
		LUdcmp lu(A);
		right(0)=y(i+4)-y(i+2);
		right(1)=y(i+3)-y(i+2);
		right(2)=y(i+1)-y(i+2);
		right(3)=y(i)-y(i+2);
		lu.solve(right,right);
		derivs(i,0)=y(i+2);
		derivs(i,1)=right(0);
		derivs(i,2)=right(1);
		derivs(i,3)=right(2);
		derivs(i,4)=right(3);
	}
	return derivs;
}

Vdoub refine(double x0, double xN, int N, double xk1, double d) {
	Vdoub x(N);
	double A, B, C, xk, S, xi, h1;
	bool sw=1;
	int i,j;
	xk=(xk1-x0)/(xN-x0);
	cout << xk << endl;
	for (j=0; j<2; j++) {
		A=((d-1)/(N-1))/(-SQR(xk)+xk-1.0/3.0);
		B=-2.0*A*xk;
		C=1.0/(N-1)+A*(xk-1.0/3.0);
		x(0)=x0;
		S=0;
		h1=99999999999;
		for (i=1; i<N; i++) {
			xi=(i+0.5)/(N-1);
			x(i)=x(i-1)+(A*SQR(xi)+B*xi+C)*(xN-x0);
			cout << "h(" << i << ")=" << x(i)-x(i-1) << endl;
			if ((x(i)>xk1) && sw==1) {
				xk=pow(i*1.0/(N-1),2.5);
				sw=0;
			}
			h1=x(i)-x(i-1);
		}
		cout << xk << endl;
		sw=1;
	}
	return x;
}
