class CSpline {
	Mdoub coef;
	int N;
public:
	CSpline(Vdoub, Vdoub);
	double Interp(double x);
	double D1 (double x);
	double D2 (double x);
};

// Constructor, creates a matrix of interpolation coefficients.

CSpline::CSpline(Vdoub x, Vdoub y) {
	if (x.size() != y.size()) throw("Different input vector sizes.");
	int i;
	N = x.size()-1;
	coef.resize(N+1,5);
	Vdoub h(N+1), u(N+1), w(N+1), v(N+1);
	for (i=0; i<N; i++)	h(i)=x(i+1)-x(i);
	for (i=0; i<N-1; i++) {
		u(i) = h(i+1)/(h(i)+h(i+1));
		w(i) = h(i)/(h(i)+h(i+1));
		v(i) = ((y(i+2)-y(i+1))/h(i+1)-(y(i+1)-y(i))/h(i))/(h(i+1)+h(i));
	}
	// Chase-away algorithm (not sure about the translation: PL przeganianie)
	Vdoub ga(N+1), be(N+1), c(N+1);
	be(1)=-u(0)/2;
	ga(1)=v(0)/2;
	for (int i=1; i<N-1; i++) {
		be(i+1)=-u(i)/(w(i)*be(i)+2);
		ga(i+1)=(v(i)-w(i)*ga(i))/(w(i)*be(i)+2);
	}
	c(N) = 0;
	for (int i=N; i>=2; i--) c(i-1) = be(i-1)*c(i)+ga(i-1);
	c(0) = 0;
	for (int i = 0; i<=(N-1); i++) {
		if (i==0) continue;
		c(i) *= 6;
	}
	// Remaining coefficients.
	Vdoub d(N+1);
	for (int i=0; i<N; i++) d(i)=(c(i+1)-c(i))/h(i);
	d(N) = -c(N)/h(N);
	Vdoub b(N+1), a(N+1);
	for (int i = 0; i<N; i++) {
		b(i)=(y(i+1)-y(i))/h(i)-c(i)*h(i)/2-(c(i+1)-c(i))*h(i)/6;
		a(i)=y(i);
	}
	for (int i=0; i<N; i++) {
		coef(i,0) = a(i);
		coef(i,1) = b(i);
		coef(i,2) = c(i);
		coef(i,3) = d(i);
	}
	for (int i=0; i<N+1; i++) coef(i,4) = x(i);
}
// Returns values interpolated for x
double CSpline::Interp(double x) {
	double Pom;
	for (int i=0; i<N; i++) {
		if ((x<=coef(i+1,4))&&(x>=coef(i,4)))	{
			Pom=coef(i,0)+coef(i,1)*(x-coef(i,4))+coef(i,2)*(x-coef(i,4))*(x-coef(i,4))/2+coef(i,3)*(x-coef(i,4))*(x-coef(i,4))*(x-coef(i,4))/6;
			break;
		}
	}
	if (x>coef(N,4)) {
		//if (x-coef(N,4)>1e-8) cout << "Ekstrapolacja w procedurze CSpline, odleglosc od ostatniego punktu = " << x-coef(N,4) << endl;
		Pom=coef(N-1,0)+coef(N-1,1)*(x-coef(N-1,4))+coef(N-1,2)*(x-coef(N-1,4))*(x-coef(N-1,4))/2+coef(N-1,3)*(x-coef(N-1,4))*(x-coef(N-1,4))*(x-coef(N-1,4))/6;
	}
	if (x<coef(0,4)) {
		//if (coef(0,4)-x>1e-8) cout << "Ekstrapolacja w procedurze CSpline, odleglosc od pierwszego punktu = " << coef(0,4)-x << endl;
		Pom=coef(0,0)+coef(0,1)*(x-coef(0,4))+coef(0,2)*(x-coef(0,4))*(x-coef(0,4))/2+coef(0,3)*(x-coef(0,4))*(x-coef(0,4))*(x-coef(0,4))/6;
	}
	return Pom;
}
// First derivative of interpolated function for x.
double CSpline::D1 (double x) {
	double Pom;
	for (int i = 0; i<=N; i++) {
		if ((x<coef(i+1,4))&&(x>=coef(i,4))) { //||x==coef[N*4+i+1]||x==coef[N*4+i]
			Pom = coef(i,1)+coef(i,2)*(x-coef(i,4))+coef(i,3)*(x-coef(i,4))*(x-coef(i,4))/2;
			break;
		}
	}
	return Pom;
}

// Second derivative of interpolated function for x.
double CSpline::D2 (double x) {
	double Pom;
	for (int i = 0; i<=N; i++) {
		if (((x<coef(i+1,4))&&(x>coef(i,4)))||x==coef(i+1,4)||x==coef(i,4))
			Pom = coef(i,2)+coef(i,3)*(x-coef(i,4));
			break;
	}
	return Pom;
}